#!/usr/bin/env bash

if [ -n "$1" ]; then
	# remove .nasm extension or a single point to ease tab completion
	NAME="${1%.c}"
	NAME="${NAME%.}"
	shift
else
	if [ -z "$DAY" ] ; then
		DAY=$(date +%-d)
	fi
	if [ -z "$YEAR" ] ; then
		YEAR=$(date +%Y)
	fi
	NAME=$YEAR-$DAY
fi

set -euo pipefail

rm -f "$NAME".o "$NAME".out

# -g for debug symbols
cc -ggdb "$NAME".c -o "$NAME".out
#ld "$NAME.o" -o "$NAME".out
"$@" ./"$NAME".out
