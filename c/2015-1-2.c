#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>

char *filename = "../inputs/2015/1";
struct stat statbuf;

int main() {
  int fd = open(filename, O_RDONLY);
  fstat(fd, &statbuf);
  char* file = mmap(0, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  close(fd);
  int floor = 0;
  int index = 0;
  file--;
  while(1) {
	if(floor < 0)
	  break;
	file++;
	index++;
	if(*file & 1)
	  floor--;
	else
	  floor++;
  }
  printf("%d\n", index);
  exit(0);
}
