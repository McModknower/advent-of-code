* McModknower's new Advent of code Repository

This repo is used by me to store my advent of code solutions.

I restarted it 2023, and during december 2023 i finished my bachelor thesis,
so there is not much here currently.

For 2022 and 2021 solutions, see the old-master branch.

Feel free to suggest (sensible) languages to attempts day 1 of 2023.
Already done in:
- Haskell
- x86_64 nasm assembly
- Rust
