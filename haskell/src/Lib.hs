module Lib (
  readYearDay
  ) where

readYearDay :: Int -> Int -> IO String
readYearDay y d = readFile $ "../inputs/" ++ show y ++ "/" ++ show d
