module AOC2023
    ( someFunc
    , d1
    -- , d2
    ) where

import qualified Lib

import qualified Data.List as L
import Data.Maybe
import Text.Parsec

someFunc :: IO ()
someFunc = putStrLn "someFunc"

r :: Int -> IO String
r = Lib.readYearDay 2023

d1 :: IO ()
d1 = do
  input <- r 1
  print $ sum $ map num $ lines input
  print $ sum $ map num2 $ lines input
    where
      num, num2 :: String -> Int
      num str = let n = filter (`elem` "0123456789") str in read [head n, last n]
      ns1 = "0123456789"
      ns2 = zip ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"] "123456789"
      scan :: String -> Maybe Char
      scan str@(x:_)
        | x `elem` ns1 = Just x
        | otherwise = listToMaybe $ map snd $ filter (\(y,_) -> y `L.isPrefixOf` str) ns2
      scan [] = Nothing
      num2 str = let n = mapMaybe scan $ L.tails str in read [head n, last n]

-- data Game = Game
--   { gn :: Int
--   , maxRed :: Int
--   , maxGreen :: Int
--   , maxBlue :: Int
--   }
-- d2 :: IO ()
-- d2 = do
--   input <- lines <$> r 2

--   return ()
--     where
--       parseGame :: Parsec String () Game
--       parseGame = do
--         _ <- string' "Game "
--         nr <- read <$> many1 digit
--         colors nr
--       colors :: Int -> Parsec String () Game
--       colors nr = do
--         char ' '
--         r <- number
