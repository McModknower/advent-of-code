data = File.read("../../inputs/2024/2")
data = data.split "\n"

data.map! { |x| x.split.map{|v| Integer(v) } }

x = data.count do |line|
  line = line[1..].zip(line).map{|a,b| a-b}
  (line.all?(&:negative?) || line.all?(&:positive?)) && line.map(&:abs).all?{|x| x<=3}
end

p x

x = data.count do |line|
  alternatives = [line]
  line.each_index { |i| alternatives.push(line.dup.tap { |y| y.delete_at(i) }) }
  alternatives.any? { |line|
    line = line[1..].zip(line).map { |a, b| a - b }
    (line.all?(&:negative?) || line.all?(&:positive?)) && line.map(&:abs).all? { |y| y <= 3 }
  }
end

p x
