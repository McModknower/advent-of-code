data = File.read('../../inputs/2024/4')

data = data.split("\n")

def search(x, y, dx, dy, str, data)
  str.each_char do |c|
    return 0 if x.negative? || y.negative?

    return 0 unless data[x] && data[x][y] == c

#    p x: x, y: y, dx: dx, dy: dy, c: data[x][y]

    x += dx
    y += dy
  end
  1
end

dirs = [[0, 1], [1, 0], [-1, 0], [0, -1], [-1, -1], [-1, 1], [1, -1], [1, 1]]

sum = 0
data.each_index do |x|
  (0...data[x].length).each do |y|
    dirs.each do |dx, dy|
      sum += search x, y, dx, dy, 'XMAS', data
    end
  end
end

p sum

def search2(x, y, data)
  return 0 unless x.positive? && y.positive? && x < data.length - 1 && y < data[0].length - 1

  return 0 unless data[x][y] == 'A'

  unless (data[x - 1][y - 1] == 'M' && data[x + 1][y + 1] == 'S') ||
         (data[x - 1][y - 1] == 'S' && data[x + 1][y + 1] == 'M')
    return 0
  end

  unless (data[x - 1][y + 1] == 'M' && data[x + 1][y - 1] == 'S') ||
         (data[x - 1][y + 1] == 'S' && data[x + 1][y - 1] == 'M')
    return 0
  end

  1
end

sum = 0
data.each_index do |x|
  (0...data[x].length).each do |y|
    sum += search2 x, y, data
  end
end

p sum
