data = File.read('../../inputs/2024/3')

re = /mul\([0-9]+,[0-9]+\)/

muls = data.scan(re).map! { |x| x.scan(/[0-9]+/).map! { |v| Integer(v) } }

res1 = muls.sum { |a, b| a * b }

p res1

re1 = /mul\([0-9]+,[0-9]+\)|don't\(\)/
re2 = /do\(\)/
i = 0
active = true
sum = 0
while i < data.length
  if active
    m = re1.match data, i
    break unless m

    i = m.offset(0)[1]
    s = m.match(0)
    if s[0] == 'm'
      # mul
      n = s.scan(/[0-9]+/).map! { |v| Integer(v) }
      sum += n[0] * n[1]
    else
      # don't()
      active = false
    end
  else
    m = re2.match data, i
    break unless m

    i = m.offset(0)[1]
    active = true
  end
end

p sum
