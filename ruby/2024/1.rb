data = File.read("../../inputs/2024/1")
data = data.split "\n"
list1 = []
list2 = []
data.each do |line|
  vals = line.split
  list1.push(Integer(vals[0]))
  list2.push(Integer(vals[1]))
end

list1.sort!
list2.sort!

sum = 0

list1.zip(list2).each do |a, b|
  diff = a - b
  diff = -diff if diff.negative?
  sum += diff
end

puts sum

last = -1
sum = 0

list1.each do |a|
  next if last == a

  last = a
  sum += a * list2.count(a)
end

puts sum
