data = File.read('../../inputs/2024/5')

(rules,books) = data.split("\n\n")

rules = rules.split("\n").map! { |r| r.split('|') }
books = books.split("\n").map! { |r| r.split(',') }

inv_rules = Hash.new([])

rules.each { |a, b| inv_rules[b] += [a] }

def check_book(book, inv_rules)
  disallowed = []
  book.each do |b|
    return false if disallowed.include? b

    disallowed += inv_rules[b]
  end
  return true
end

# copied from https://docs.ruby-lang.org/en//3.2/TSort.html
require 'tsort'
class Hash
  include TSort
  alias tsort_each_node each_key
  def tsort_each_child(node, &block)
    fetch(node).each(&block)
  end
end
# end of copy

forw_rules = Hash.new([])
rules.each { |a, b| forw_rules[a] += [b] }

def order(book, forw_rules)
  forw_rules = forw_rules.select { |k, _| book.include? k }
  forw_rules.transform_values! { |v| v.filter { |p| book.include? p } }
  forw_rules.tsort
end

def middle(list)
  list[list.length / 2]
end

sum1 = 0
sum2 = 0

books.each do |book|
  if check_book(book, inv_rules)
    sum1 += Integer(middle(book))
  else
    sum2 += Integer(middle(order(book, forw_rules)))
  end
end

p sum1
p sum2
