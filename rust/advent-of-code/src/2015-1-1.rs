use std::fs;
//use std::str::Chars;
//use std::collections::HashMap;

fn main() {
	let contents = fs::read_to_string("../../inputs/2015/1")
		.expect("Could not read input file");
	let mut floor = 0;
	for c in contents.chars() {
		if c == '(' {
			floor += 1;
		} else {
			floor -= 1;
		}
	}
    print!("{}", floor);
}
