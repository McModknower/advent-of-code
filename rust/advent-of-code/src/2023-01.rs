use std::fs;
use std::str::Chars;
use std::collections::HashMap;

fn main() {
	let contents = fs::read_to_string("../../inputs/2023/1")
		.expect("Could not read input file");
	let lines = contents.lines();
	let (result1, result2) = lines
		.map(|line| (p1(line.chars()), p2(line)))
		.fold((0,0), |(a1,a2), (x1,x2)| (a1+x1, a2+x2));
    println!("{}", result1);
    println!("{}", result2);
}

fn p1(line: Chars<'_>) -> u32 {
	let nums = line
		.filter_map(|c| c.to_digit(10))
		.collect::<Vec<_>>();
	nums.first().expect("line without nums") * 10 + nums.last().expect("line without nums")
}

fn p2(line: &str) -> u32 {
	let len = line.len();
	let nums = (0..len)
		.filter_map(|i| line.get(i..))
		.filter_map(|x| p2help(x))
		.collect::<Vec<_>>();
	nums.first().expect("line without nums") * 10 + nums.last().expect("line without nums")
}

fn p2help(text: &str) -> Option<u32> {
	let numbers :HashMap<&str, u32> = HashMap::from([
		("one", 1),
		("two", 2),
		("three", 3),
		("four", 4),
		("five", 5),
		("six", 6),
		("seven", 7),
		("eight", 8),
		("nine", 9),
	]);
	let fst = text.chars().next().expect("no char");
	if fst.is_digit(10) {
		return fst.to_digit(10);
	}
	for (key, val) in numbers.iter() {
		if text.starts_with(key) {
			return Some(*val)
		}
	}
	return None;
}
