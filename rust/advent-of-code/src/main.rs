use std::fs;
//use std::str::Chars;
//use std::collections::HashMap;

fn main() {
	let contents = fs::read_to_string("../../inputs/2015/1")
		.expect("Could not read input file");
	let mut floor = 0;
	let mut index = 0;
	for c in contents.chars() {
		index += 1;
		if c == '(' {
			floor += 1;
		} else {
			if floor == 0 {
				break;
			}
			floor -= 1;
		}
	}
    print!("{}", index);
}
