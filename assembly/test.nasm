%include "lib.nasm"

section .data:
	filename db "test.nasm",0

section .text:
	global _start

_start:
	mov rdi, 0
	call printi
	mov rdi, 42
	call printi
	mov rdi, 0
	call printi

	mov rdi, 42
	call malloc

	lea rdi, [filename]
	call loadfile

	cmp byte [rax], '%'
	jnz _start_no_percent

	add rax, rsi
	dec rax
	test byte [rax], 0xFF
	jnz _start_not_zero_padded

	xor rdi, rdi
	call exit

_start_no_percent:
	mov rdi, 2
	call exit
_start_not_zero_padded:
	mov rdi, 1
	call exit
