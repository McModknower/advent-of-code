%include "lib.nasm"

;; A file used for temporary testing of stuff.
;; please ignore this unless you wanna analyse random stuff

section .data
    filename db "tmp.nasm",0

section .text
    extern malloc
    global _start

_start:

    mov rdi, 42
    call malloc

	mov rdi,rax
	call printi

    xor rdi, rdi
    mov rax, 60
    syscall

a:
.t:
	ret

b:
.t:
	ret
