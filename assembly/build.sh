#!/usr/bin/env bash

if [ -n "$1" ]; then
	# remove .nasm extension or a single point to ease tab completion
	NAME="${1%.nasm}"
	NAME="${NAME%.}"
	shift
else
	if [ -z "$DAY" ] ; then
		DAY=$(date +%-d)
	fi
	if [ -z "$YEAR" ] ; then
		YEAR=$(date +%Y)
	fi
	NAME=$YEAR-$DAY
fi

set -euo pipefail

if ! [ -e sizes-and-offsets ]; then
	./initSystem.sh
fi

rm -f "$NAME".o "$NAME".out

# -g for debug symbols
nasm -f elf64 -g "$NAME".nasm -o "$NAME".o
ld "$NAME.o" -o "$NAME".out
"$@" ./"$NAME".out
