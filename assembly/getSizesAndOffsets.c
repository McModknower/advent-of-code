#include <stdio.h>

#include <sys/stat.h>

// used for offsetof
#include <stddef.h>

// used for mmap
#include <sys/mman.h>

#include <error.h>

char* register_prefix(size_t size) {
  if (size==4) {
	return "e %+";
  } else if (size==8) {
	return "r %+";
  } else {
    return "";
  }
}

char* register_suffix(size_t size) {
  if (size==1) {
	return "%+ l";
  } else {
	return "";
  }
}


int main() {
  printf("%%assign STATBUF_SIZE %lu\n", sizeof(struct stat));
  printf("%%assign ST_DEV_OFFSET %lu\n", offsetof(struct stat, st_dev));
  printf("%%assign ST_INO_OFFSET %lu\n", offsetof(struct stat, st_ino));
  printf("%%assign ST_MODE_OFFSET %lu\n", offsetof(struct stat, st_mode));
  printf("%%assign ST_NLINK_OFFSET %lu\n", offsetof(struct stat, st_nlink));
  printf("%%assign ST_UID_OFFSET %lu\n", offsetof(struct stat, st_uid));
  printf("%%assign ST_GID_OFFSET %lu\n", offsetof(struct stat, st_gid));
  printf("%%assign ST_RDEV_OFFSET %lu\n", offsetof(struct stat, st_rdev));
  printf("%%assign ST_SIZE_OFFSET %lu\n", offsetof(struct stat, st_size));


  printf("%%assign PROT_READ %u\n", PROT_READ);
  printf("%%assign PROT_WRITE %u\n", PROT_WRITE);
  printf("%%assign PROT_EXEC %u\n", PROT_EXEC);

  printf("%%assign MAP_SHARED %u\n", MAP_SHARED);
  printf("%%assign MAP_PRIVATE %u\n", MAP_PRIVATE);
  printf("%%assign MAP_ANONYMOUS %u\n", MAP_ANONYMOUS);

  printf("%%define REG_OFF_T(reg) %s reg %s\n", register_prefix(sizeof(off_t)), register_suffix(sizeof(off_t)));
  /* printf("-DREG_PREFIX_SIZE_T=%s\n", register_prefix(sizeof(size_t))); */
  /* printf("-DREG_SUFFIX_SIZE_T=%s\n", register_suffix(sizeof(size_t))); */
}
