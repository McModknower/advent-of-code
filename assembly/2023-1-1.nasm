%include "lib.nasm"

section .rodata
	filename db "../inputs/2023/1",0

section .text
global _start

_start:
	lea rdi, [filename]
	call loadfile
	; now rax points to start of file

	; idea:
	;  init result with 0
	;  loop over lines
	;   init first, last with 0
	;   loop single line
	;    if current is number
	;     if first is 0
	;      set first to current
	;     set last to current
	;   multiply first by 10
	;   add second to first
	;   add first to result
	;  printi result

	; init result with 0
	mov rdi, 0
	; loop over lines (there will be at least one)
lines_loop:
	; init first and last to 0
	mov rbx, 0
	mov rcx, 0
	; loop over single line (will not be empty)
line_loop:
	mov dl, [rax]
	inc rax
	cmp dl, `\n`
	je line_loop_end
	; check if digit
	cmp dl, '0'
	jl line_loop
	cmp dl, '9'
	jg line_loop
	; mask out higher bits to get numeric value
	and dl, 0x0F
	; only set rbx if it is zero
	test bl, bl
	jnz no_first
	; set second always to get last value
	mov bl, dl
no_first:
	mov cl, dl
	jmp line_loop
line_loop_end:
	; sum values
	imul rbx, 10
	add rcx, rbx
	add rdi, rcx
	; check for eof, marked with 0 by loadfile
	test byte [rax], 0xFF
	jnz lines_loop

	call printi

	mov rdi, 0
	call exit
