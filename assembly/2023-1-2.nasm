%include "lib.nasm"

section .rodata
	filename db "../inputs/2023/1",0
	numbers db "one",0,1,"two",0,2,"three",0,3,"four",0,4,"five",0,5,"six",0,6,"seven",0,7,"eight",0,8,"nine",0,9,0

section .text
global _start

_start:
	lea rdi, [filename]
	call loadfile
	; now rax points to start of file

	; idea:
	;  init result with 0
	;  loop over lines
	;   init first, last with 0
	;   loop single line
	;    if current is number
	;     if first is 0
	;      set first to current
	;     set last to current
	;   multiply first by 10
	;   add second to first
	;   add first to result
	;  printi result

	; init result with 0
	mov r8, 0
	; save pointer in r9
	mov r9, rax
	; loop over lines (there will be at least one)
	dec r9
.lines_loop:
	; init first and last to 0
	mov rbx, 0
	mov rcx, 0
	; loop over single line (will not be empty)
.line_loop:
	inc r9
	mov dl, [r9]
	cmp dl, `\n`
	je .line_loop_end
	; check if digit
	cmp dl, '0'
	jl .no_digit
	cmp dl, '9'
	jg .no_digit
	; mask out higher bits to get numeric value
	and dl, 0x0F
	jmp .num
.no_digit:
	lea rdi, [numbers]
.no_digit_loop:
	mov rsi, r9
	call strpref
	test rax,rax
	; it is this number
	jnz .strnum
	; it is not this number
	; advance number pointer
	call strend
	add rdi, 2
	test byte [rdi], 0xFF
	; if at end of number list, go to next char
	jz .line_loop
	jmp .no_digit_loop
.strnum:
	; rax is number
	mov dl, al
.num:
	; only set rbx if it is zero
	test bl, bl
	jnz .no_first
	; set second always to get last value
	mov bl, dl
.no_first:
	mov cl, dl
	jmp .line_loop
.line_loop_end:
	; sum values
	imul rbx, 10
	add rcx, rbx
	add r8, rcx
	; check for eof, marked with 0 by loadfile
	test byte [r9+1], 0xFF
	jnz .lines_loop

	mov rdi, r8
	call printi

	mov rdi, 0
	call exit

;; check if rsi starts with rdi
;; if no, return 0
;; if yes, return byte after nullbyte of rdi
;; on return rsi and rdi point at the first point
;; where the strings differ
;; or on the nullbyte of rdi and after the prefix in rsi
strpref:
	test byte [rdi], 0xFF
	jnz .snz
	; prefix ended, is prefix
	mov rax, [rdi+1]
	ret
.snz:
	mov al,[rsi]
	test al, al
	; string ended, is not prefix
	jnz .fnz
	mov rax,0
	ret
.fnz:
	; rax is still char of string/rsi
	cmp al, [rdi]
	je .eq
	; chars not equal, fail
	mov rax,0
	ret
.eq:
	; chars are equal, continue
	inc rdi
	inc rsi
	jmp strpref

;; advance pointer rdi to the next nullbyte, but at least one byte
strend:
	inc rdi
	test byte [rdi], 0xFF
	jnz strend
	ret
