#!/usr/bin/env bash

set -euo pipefail

cc -o getSizesAndOffsets.out getSizesAndOffsets.c
./getSizesAndOffsets.out > sizes-and-offsets
