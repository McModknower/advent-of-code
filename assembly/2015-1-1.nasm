%include "lib.nasm"

section .rodata
	filename db "../inputs/2015/1",0

section .text
global _start

_start:
	lea rdi, [filename]
	call loadfile
	; now rax points to start of file

	; init result with 0
	mov rdi, 0

	dec rax
	; loop over chars (there will be at least one)
.chars_loop:

	inc rax
	; check for eof, marked with 0 by loadfile
	test byte [rax], 0xFF
	jz .end

	test byte [rax], 1
	jnz .dec

	inc rdi
	jmp .chars_loop

.dec:
	dec rdi
	jmp .chars_loop

.end:
	call printi

	mov rdi, 0
	call exit
