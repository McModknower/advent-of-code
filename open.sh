#! /bin/bash
BASEURL="https://adventofcode.com"

cd "$(dirname "$0")"

if [ -z "$DAY" ] ; then
	DAY=$(date +%-d)
fi
if [ -z "$YEAR" ] ; then
	YEAR=$(date +%Y)
fi

URL=$BASEURL/$YEAR/day/$DAY
export DISPLAY=:0
export XDG_RUNTIME_DIR=/run/user/1000
xdg-open "$URL"

./getInput.sh
