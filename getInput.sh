#!/usr/bin/env bash

cd "$(dirname "$0")"
source .env

if [ -z "$DAY" ] ; then
	DAY=$(date +%-d)
fi
if [ -z "$YEAR" ] ; then
	YEAR=$(date +%Y)
fi

URL=https://adventofcode.com/$YEAR/day/$DAY/input
FILENAME=./inputs/$YEAR/$DAY
mkdir -p "$(dirname "$FILENAME")"

wget --header='Cookie: '"$COOKIE"\
     --user-agent='mcmodknower aka teetoll [at] t-online [dot] de via curl'\
     $URL -O $FILENAME
